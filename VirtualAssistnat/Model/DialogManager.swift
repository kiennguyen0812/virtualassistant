//
//  DialogManager.swift
//  VirtualAssistnat
//
//  Created by Kien Nguyen on 06.03.2022.
//

import Foundation
import Alamofire

protocol DialogManagerDelegate {
    func didReceiveDialog(_ dialogManager:DialogManager, recievedText: String)
}
class DialogManager {
    var session_id = String(Int(NSDate().timeIntervalSince1970))
    let token = Keys.tokenDialog
    let urlString = Keys.urlDialog
    var isQuestion = false
    var delegate: DialogManagerDelegate?
    func postDialog(responseASR:String, completionHandler: @escaping ([String:Bool], NSError?) -> ()) {
        let headers:HTTPHeaders = [
                "token":token,
            ]
        struct params: Encodable {
            let message: String
            let get_debug: Bool
        }

        let parameters = [
            "message": responseASR,
            "get_debug": "true",
        ]
        let urlString = "\(urlString)\(session_id)/messages"
        print("Check URL \(urlString)")
        AF.request(urlString, method: .post, parameters:parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{ [self](response) in
                let responseDialog = self.parseDialogJSON(dialog: response.data!)
                self.isQuestion = self.checkQuestion(dialog: response.data!)
                print("Check isQuestion \(self.isQuestion)")
                self.delegate?.didReceiveDialog(self, recievedText: responseDialog)
                completionHandler([responseDialog:self.isQuestion],nil)
            }
    }
    func checkQuestion(dialog:Data) -> Bool {
        let decoder = JSONDecoder()
        do{
            let decoderData = try decoder.decode(Dialog.self, from: dialog)
            return decoderData.data.dialog_response.debug_data.hasMissing
        }catch{
            print(error)
            return false
        }
    }
    func parseDialogJSON(dialog:Data) -> String{
        let decoder = JSONDecoder()
        do{
            let decoderData = try decoder.decode(Dialog.self, from: dialog)
            if(decoderData.data.bot_message.indices.contains(1)){
                print(decoderData.data.bot_message[1].value)
                return decoderData.data.bot_message[0].value
            }else{
                return decoderData.data.bot_message[0].value
            }
        }catch{
            print(error)
            return ""
        }
    }
    func updateSentencesDialog(responseASR:String, completionHandler: @escaping ([String:Bool], NSError?)->()){
        postDialog(responseASR: responseASR, completionHandler: completionHandler)
    }

}

