//
//  Detail.swift
//  VirtualAssistnat
//
//  Created by Kien Nguyen on 04.03.2022.
//

import Foundation
struct Detail : Decodable{
    let type: String
    let value: String
}
