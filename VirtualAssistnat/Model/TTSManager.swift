//
//  TTSManager.swift
//  VirtualAssistnat
//
//  Created by Kien Nguyen on 06.03.2022.
//

import Foundation
import Alamofire
struct TTSManager {
    let token = Keys.tokenTTS
    let urlString = Keys.urlTTS
    
    func postTTS(responseDialog:String,completionHandler: @escaping (String, NSError?) -> ()) {
        let headers:HTTPHeaders = [
            "token":token
            ]
        let parameters = [
            "text": responseDialog,
            "language_code": "vi_vn",
            "voice_name":"female_south2",
            "generator":"melgan",
            "acoustic_model":"fastspeech2",
            "style":"news",
            "output_format": "mp3"
        ]
        let urlString = urlString
        AF.request(urlString, method: .post, parameters:parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{(response) in
                let responseParagraph = parseTtsJSON(tts: response.data!)
                completionHandler(responseParagraph, nil)
            }
    }
    func parseTtsJSON(tts:Data)->String{
        let decoder = JSONDecoder()
        do{
            let decoderData = try decoder.decode(TTS.self, from: tts)
            return decoderData.data.paragraph
        }catch{
            print(error)
            return ""
        }
    }
    func updateTTS(responseDialog:String, completionHandler: @escaping (String, NSError?)->()){
        postTTS(responseDialog: responseDialog, completionHandler: completionHandler)
    }
}
