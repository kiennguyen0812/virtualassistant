//
//  Message.swift
//  VirtualAssistnat
//
//  Created by Bùi Quang Hiếu on 12/03/2022.
//

import Foundation
struct Message {
    let sender: String
    let bodyText: String
}
