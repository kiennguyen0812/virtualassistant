//
//  TTS.swift
//  VirtualAssistnat
//
//  Created by Kien Nguyen on 04.03.2022.
//

import Foundation
//Struct of response TTS
struct TTS: Decodable{
    let data: DataTTS
}
struct DataTTS: Decodable{
    let paragraph: String
    let sentences: Array<String>
}

