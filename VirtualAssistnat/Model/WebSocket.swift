//
//  WebSocket.swift
//  VirtualAssistnat
//
//  Created by Kien Nguyen on 05.03.2022.
//

import Foundation
import Starscream
protocol WebSocketCustomDelegate {
    func checkIsConnecting(_ websocket: WebSocketConnection, isConnect: Bool)
}
class WebSocketConnection: WebSocketDelegate{

    
    var socket: WebSocket!
    var isConnected = false
    var request = URLRequest(url: URL(string: Keys.urlASR)!)
    var token = Keys.tokenASR
    var device_id = "idDevice"
    var session_id = String(Int(NSDate().timeIntervalSince1970))
    var forward_message = "false"
    var partial : String?
    var autoTriggerStop = false
    func connect(){
        request.timeoutInterval = 5
        request.setValue(token, forHTTPHeaderField: "token")
        request.setValue(device_id, forHTTPHeaderField: "device_id")
        request.setValue(session_id, forHTTPHeaderField: "session_id")
        request.setValue(forward_message, forHTTPHeaderField: "forward_message")

        socket = WebSocket(request: request)
        socket.delegate = self
        socket.connect()
    }
    var delegate: WebSocketCustomDelegate?
    
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
               case .connected(let headers):
                   isConnected = true
                   print("websocket is connected: \(headers)")
               case .disconnected(let reason, let code):
                   isConnected = false
                   self.delegate?.checkIsConnecting(self, isConnect: isConnected)
                   print("websocket is disconnected: \(reason) with code: \(code)")
               case .text(let string):
                   print("Received text: \(string)")
                   let tempPartial = parseAsrJSON(asr: string)
                   if(tempPartial != ""){
                       partial = tempPartial
                   }else{
                       autoTriggerStop = true
                   }
                   print("Decode data: \(partial ?? "")")
               case .binary(let data):
                   print("Received data: \(data.count)")
               case .ping(_):
                   break
               case .pong(_):
                   break
               case .viabilityChanged(_):
                   break
               case .reconnectSuggested(_):
                   break
               case .cancelled:
                   isConnected = false
               case .error(let error):
                   isConnected = false
                   handleError(error)
               }
    }
    func handleError(_ error: Error?) {
            if let e = error as? WSError {
                print("websocket encountered an error: \(e.message)")
            } else if let e = error {
                print("websocket encountered an error: \(e.localizedDescription)")
            } else {
                print("websocket encountered an error")
            }
        }
    func disconnect() {
         if isConnected {
             writeEOF()
             socket.disconnect()
             print("Websocket is disconnected")
         } else {
             socket.connect()
         }
     }
    func writeData(data:Data) {
          socket.write(data: data)
    }
    func writeEOF(){
        socket.write(string:"EOF")
    }
    func parseAsrJSON(asr:String)->String{
        let asrData = asr.data(using: .utf8)
        let decoder = JSONDecoder()
        let exceptionText = ""
        do{
            let decoderData = try decoder.decode(TextASR.self, from: asrData!)
//            exceptionText = decoderData.text
            return decoderData.partial
        }catch{
            print(error)
            return exceptionText
        }
    }
    func getPartial()->String{
        return partial ?? ""
    }
     
}
