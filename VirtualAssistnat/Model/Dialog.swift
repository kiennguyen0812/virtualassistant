//
//  Dialog.swift
//  VirtualAssistnat
//
//  Created by Kien Nguyen on 04.03.2022.
//

import Foundation

struct Dialog:Decodable {
    let data: DataDialog
}
struct Detail : Decodable{
    let type: String
    let value: String
}
struct DataDialog:Decodable{
    let bot_message: Array<Detail>
    let dialog_response: DialogResponse
}
struct DebugData:Decodable{
    let hasMissing: Bool
}
struct DialogResponse: Decodable{
    let debug_data: DebugData
}
