//
//  MessageCell.swift
//  VirtualAssistnat
//
//  Created by Bùi Quang Hiếu on 12/03/2022.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var rightImageView: UIImageView!
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var messageBuble: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        print("Waken up")
        messageBuble.layer.cornerRadius = messageBuble.frame.size.height / 5

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
