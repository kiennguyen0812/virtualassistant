//
//  ViewController.swift
//  VirtualAssistnat
//
//  Created by Kien Nguyen on 02.03.2022.
//

import UIKit
import Porcupine
import Alamofire
import AVFoundation

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var buttonRecord: UIButton!
    @IBOutlet weak var buttonRecord2: UIButton!
    @IBOutlet weak var labelTextASR: UILabel!
    
    let accessKey = Keys.accesskey_pocupine
    var wakeWord = Porcupine.BuiltInKeyword.heyGoogle
    var porcupineManager: PorcupineManager!

    var timer = Timer()
    var counter = 0
    var isPlaying = false
    
    var sentenceResponseDialog : String!
    var player : AVPlayer?
    var playerItem:AVPlayerItem?
    
    var audioEngine = AVAudioEngine()
    var webSocketConnecttion = WebSocketConnection()
    var textASR : String?
    
    var dialogManager = DialogManager()
    var ttsManager = TTSManager()
    
    var audioPlayer = AVAudioPlayer()

    var checkQuestion = false
 
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        webSocketConnecttion.delegate = self
        do
        {
            let keywordCallback: ((Int32) -> Void) = { [self]_ in
                print("Callback is triggered")
                counter = 0
                self.timer.invalidate()
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
                isPlaying = true
                webSocketConnecttion.connect()
                playSoundNotification()
                startRecord()
            }
            self.porcupineManager = try PorcupineManager(accessKey: accessKey, keyword: wakeWord, sensitivity: 0.8, onDetection: keywordCallback)
            try porcupineManager.start()
        }
        catch let error as PorcupineInvalidArgumentError {
             print("Ensure your accessKey '\(accessKey)' is valid")
         } catch is PorcupineActivationError {
             print("AccessKey activation error")
         } catch is PorcupineActivationRefusedError {
             print("AccessKey activation refused")
         } catch is PorcupineActivationLimitError {
             print("AccessKey reached its limit")
         } catch is PorcupineActivationThrottledError  {
             print("AccessKey is throttled")
         } catch let error {
             print("\(error)")
         }
    }
    
    @IBAction func pressedButton2(_ sender: UIButton){
        if(!isPlaying){
            isPlaying = true
            counter = 0
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
            print("Playing")
            webSocketConnecttion.connect()
            startRecord()

        }else{
            alreadyGotData()
        }
    }
    @objc func timerAction() {
        counter += 1
        label.text = "\(counter)"
    }
    
   func alreadyGotData(){
       isPlaying = false
       audioEngine.stop()
       textASR = webSocketConnecttion.getPartial()
       webSocketConnecttion.autoTriggerStop = false
       webSocketConnecttion.disconnect()
       print("Stop Playing")
       timer.invalidate()
       Timer.scheduledTimer(timeInterval: 3, target: self, selector: "wait", userInfo: nil, repeats: false)
       clearTextInLabel()
       print("Check text ASR \(textASR)")

       do{
           try dialogManager.postDialog(responseASR: textASR ?? "" , completionHandler: {responseDialog, errorCallback in
               let result = responseDialog
               self.checkQuestion = responseDialog[Array(result.keys)[0]]!
               print("After calling API, response text is \(result)")
               self.ttsManager.postTTS(responseDialog: Array(result.keys)[0], completionHandler: {responseParagraph, errorCallback in
                   print("After calling API, response paragraph is \(responseParagraph)")
                   let url_tts = URL(string: "https://dev.vinbase.ai/api/v1/tts/results/mp3/"+responseParagraph)
                   DispatchQueue.global().async {
                       self.downloadFileFromURL(url: url_tts!)
                   }
                   print("On API isQuestion: \(self.checkQuestion)")
                   
                   

               })
           })

       }
       catch let error{
           print("\(error)")
       }
    
    }
    @IBAction func chatbotPressed(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: Keys.identifier, sender: self)
    }
    func startRecord() {
        playSoundNotification()
        audioEngine = AVAudioEngine()
        let bus = 0
        let inputNode = audioEngine.inputNode
        let inputFormat = inputNode.outputFormat(forBus: bus)

        var streamDescription = AudioStreamBasicDescription()
        streamDescription.mSampleRate = 16000.0
        streamDescription.mFormatID = kAudioFormatLinearPCM
        streamDescription.mFormatFlags = kAudioFormatFlagIsSignedInteger // no endian flag means little endian
        streamDescription.mBytesPerPacket = 2
        streamDescription.mFramesPerPacket = 1
        streamDescription.mBytesPerFrame = 2
        streamDescription.mChannelsPerFrame = 1
        streamDescription.mBitsPerChannel = 16
        streamDescription.mReserved = 0


        let outputFormat = AVAudioFormat(streamDescription: &streamDescription)!

        guard let converter: AVAudioConverter = AVAudioConverter(from: inputFormat, to: outputFormat) else {
            print("Can't convert in to this format")
            return
        }

        inputNode.installTap(onBus: 0, bufferSize: 1024, format: inputFormat) { [self] (buffer, time) in
//            print("Buffer format: \(buffer.format)")

            var newBufferAvailable = true

            let inputCallback: AVAudioConverterInputBlock = { inNumPackets, outStatus in
                if newBufferAvailable {
                    outStatus.pointee = .haveData
                    newBufferAvailable = false
                    return buffer
                } else {
                    outStatus.pointee = .noDataNow
                    return nil
                }
            }

            let convertedBuffer = AVAudioPCMBuffer(pcmFormat: outputFormat, frameCapacity: AVAudioFrameCount(outputFormat.sampleRate) * buffer.frameLength / AVAudioFrameCount(buffer.format.sampleRate))!

            var error: NSError?
            let status = converter.convert(to: convertedBuffer, error: &error, withInputFrom: inputCallback)
            assert(status != .error)
            
            let bufferData = self.audioBufferToData(audioBuffer: convertedBuffer)
            self.webSocketConnecttion.writeData(data: bufferData)
//            print("Converted buffer format:", convertedBuffer.format)
//            print("Check auto trigger stop: \(webSocketConnecttion.autoTriggerStop)")
            let tempPartial = webSocketConnecttion.getPartial()
            fillTextInLabel(textASR: tempPartial)
            
            if(webSocketConnecttion.autoTriggerStop){
                alreadyGotData()
                print("After call API, isQuestion \(checkQuestion)")
            }
        }

        audioEngine.prepare()

        do {
            try audioEngine.start()
        } catch {
            print("Can't start the engine: \(error)")
        }

    }
    func audioBufferToData(audioBuffer: AVAudioPCMBuffer) -> Data {
        let channelCount = 1
        let bufferLength = (audioBuffer.frameCapacity * audioBuffer.format.streamDescription.pointee.mBytesPerFrame)
        
        let channels = UnsafeBufferPointer(start: audioBuffer.int16ChannelData, count: channelCount)
        let data = Data(bytes: channels[0], count: Int(bufferLength))

        return data
    }
    func fillTextInLabel(textASR:String){
        DispatchQueue.main.async {
            self.labelTextASR.isHidden = false
            self.labelTextASR.layer.cornerRadius = 20
            self.labelTextASR.layer.masksToBounds = true
            self.labelTextASR.backgroundColor = UIColor.white
            self.labelTextASR.textColor = UIColor(named: "MyGreen")
            self.labelTextASR.text = textASR
        }
    }
    func clearTextInLabel(){
        DispatchQueue.main.async {
            self.labelTextASR.text = ""
            self.labelTextASR.backgroundColor = UIColor.clear
            self.labelTextASR.isHidden = true

        }
    }
    func playSoundNotification(){
        let path = Bundle.main.path(forResource: "sound", ofType: "wav")!
        let url = URL(fileURLWithPath: path)

        do {
            //create your audioPlayer in your parent class as a property
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer.play()
        } catch {
            print("couldn't load the file")
        }
    }
    
    func downloadFileFromURL(url:URL){
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url, completionHandler: { [weak self](URL, response, error) -> Void in
            self?.play(url: URL!, url_initial: url)
        })
            
        downloadTask.resume()
    }
    func play(url:URL, url_initial:URL) {
        do {
            print("URL HERE \(url)")
            audioPlayer = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            audioPlayer.volume = 1.0
            audioPlayer.play()
        } catch let error as NSError {
            //self.player = nil
            print("Error ne ", error.localizedDescription)
            print("url catch \(url_initial)")
            downloadFileFromURL(url: url_initial)
        } catch {
            print("AVAudioPlayer init failed")
        }
    }
}
extension ViewController: AVAudioPlayerDelegate{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if flag == true{
            print("Audio was played")
            print("Check is Question \(checkQuestion)")
            if checkQuestion{
                counter = 0
                timer.invalidate()
                timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
                webSocketConnecttion.connect()
                startRecord()
            }
        }
    }
}
extension ViewController: WebSocketCustomDelegate{
    func checkIsConnecting(_ websocket: WebSocketConnection, isConnect: Bool) {
        if(isConnect == false){
            print("Websocket is disconnected")
            timer.invalidate()
            isPlaying = false
            audioEngine.stop()
            clearTextInLabel()
            ttsManager.postTTS(responseDialog: "Vui lòng nhắc lại, tôi chưa nghe rõ bạn", completionHandler: {responseParagraph, errorCallback in
                print("After calling API, response paragraph is \(responseParagraph)")
                let url_tts = URL(string: "https://dev.vinbase.ai/api/v1/tts/results/mp3/"+responseParagraph)
                DispatchQueue.global().async {
                    self.downloadFileFromURL(url: url_tts!)
                }
            })
        }
    }
}


