//
//  ChatViewController.swift
//  VirtualAssistnat
//
//  Created by Bùi Quang Hiếu on 12/03/2022.
//

import UIKit

class ChatViewController: UIViewController {

    @IBOutlet weak var chatTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var dialogManager = DialogManager()
    var messages : [Message] = [
        Message(sender: "bot", bodyText: "Quý khách muốn hỏi gì nào"),
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.register(UINib(nibName: Keys.cellNibName, bundle: nil), forCellReuseIdentifier: Keys.cellIdentifier)
        chatTextField.autocorrectionType = UITextAutocorrectionType.no
        loadData()

        // Do any additional setup after loading the view.
    }
    func loadData(){
        tableView.reloadData()
        if self.messages.count > 1{
            let indexPath = IndexPath(row: self.messages.count - 1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
    
    @IBAction func sendPressed(_ sender: UIButton) {
        if let messageBody = chatTextField.text{
            print("Message Input: ", messageBody)
            let message = Message(sender: "user", bodyText: messageBody)
            messages.append(message)
            print("Message number of eles: \(messages.count)")
            dialogManager.postDialog(responseASR: messageBody, completionHandler: {responseDialog, errorCallback in
                let resultTest = Array(responseDialog.keys)[0]
                print("Response text \(resultTest)")
                let messageResponse = Message(sender: "bot", bodyText: resultTest)
                self.messages.append(messageResponse)
                DispatchQueue.main.async{
                    self.chatTextField.text = ""
                    self.loadData()
                }
            })
              
        }
        
    }
//    Thời tiết hôm nay thế nào
}
extension ChatViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Message Count: ",messages.count)
        return messages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: Keys.cellIdentifier, for: indexPath) as! MessageCell
        cell.label.text = message.bodyText
        if message.sender == "user" {
            cell.leftImageView.isHidden = true
            cell.rightImageView.isHidden = false
            cell.messageBuble.backgroundColor = UIColor.blue
            cell.label.textColor = UIColor.white
        }else{
            cell.leftImageView.isHidden = false
            cell.rightImageView.isHidden = true
            cell.messageBuble.backgroundColor = UIColor.yellow
            cell.label.textColor = UIColor.black

        }
        return cell
    }
}
